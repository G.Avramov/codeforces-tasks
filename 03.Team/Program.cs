﻿using System;
using System.Linq;

namespace _03.Team
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfProblems = int.Parse(Console.ReadLine());

            int counter = 0;

            for (int i = 0; i < numberOfProblems; i++)
            {
                int[] arr = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

                if (arr.Sum() >= 2)
                {
                    counter++;
                }
            }

            Console.WriteLine(counter);
        }
    }
}
