﻿using System;

namespace _11.StonesOnTheTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfStones = int.Parse(Console.ReadLine());

            string stones = Console.ReadLine();

            Console.WriteLine(StonesToTake(stones));
        }

        private static int StonesToTake(string stones)
        {
            string result = "";

            result += stones[0];

            for (int i = 0; i < stones.Length - 1; i++)
            {
                if (stones[i] != stones[i + 1])
                {
                    result += stones[i + 1];
                }
            }

            return(stones.Length - result.Length);
        }
    }
}
