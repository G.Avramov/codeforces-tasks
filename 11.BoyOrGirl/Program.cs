﻿using System;
using System.Linq;

namespace _11.BoyOrGirl
{
    class Program
    {
        static void Main(string[] args)
        {
            string username = Console.ReadLine();

            char[] chars = username.ToCharArray();

            char[] distinct = chars.Distinct().ToArray();

            if(distinct.Length % 2 == 0)
            {
                Console.WriteLine("CHAT WITH HER!");
            }
            else
            {
                Console.WriteLine("IGNORE HIM!");
            }
        }
    }
}
