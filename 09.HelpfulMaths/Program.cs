﻿using System;
using System.Linq;

namespace _09.HelpfulMaths
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = Console.ReadLine()
                .Split('+')
                .Select(int.Parse)
                .ToArray();

            BubbleSort(numbers);
            
            Console.WriteLine(string.Join("+", numbers));
        }

        public static void BubbleSort(int[] input)
        {
            var itemMoved = false;
            do
            {
                itemMoved = false;
                for (int i = 0; i < input.Count() - 1; i++)
                {
                    if (input[i] > input[i + 1])
                    {
                        var lowerValue = input[i + 1];
                        input[i + 1] = input[i];
                        input[i] = lowerValue;
                        itemMoved = true;
                    }
                }
            } while (itemMoved);
        }
    }
}
