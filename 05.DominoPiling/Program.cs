﻿using System;
using System.Linq;

namespace _05.DominoPiling
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

            int boardSize = arr[0] * arr[1];

            int piecesCount = 0;

            while (boardSize > 0)
            {
                if (boardSize == 1)
                {
                    break;
                }

                boardSize -= 2;

                piecesCount++;
            }

            Console.WriteLine(piecesCount);
        }
    }
}
