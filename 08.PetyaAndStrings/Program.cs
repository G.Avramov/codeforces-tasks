﻿using System;

namespace _08.PetyaAndStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstStr = Console.ReadLine().ToLower();
            string secondStr = Console.ReadLine().ToLower();

            int result = 0;

            for (int i = 0; i < firstStr.Length; i++)
            {
                if (firstStr[i] != secondStr[i])
                {
                    if (firstStr[i] < secondStr[i])
                    {
                        result = -1;
                        break;
                    }
                    else
                    {
                        result = 1;
                        break;
                    }
                }
            }

            Console.WriteLine(result);
        }
    }
}
