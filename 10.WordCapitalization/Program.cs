﻿using System;

namespace _10.WordCapitalization
{
    class Program
    {
        static void Main(string[] args)
        {
            string word = Console.ReadLine();

            Console.WriteLine(FirstLetterToUpper(word));
        }

        private static string FirstLetterToUpper(string s)
        {
            return s[0].ToString().ToUpper() + s.Substring(1);
        }
    }
}
