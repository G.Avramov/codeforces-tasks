﻿using System;

namespace _15.Word
{
    class Program
    {
        static void Main(string[] args)
        {
            string word = Console.ReadLine();

            Console.WriteLine(ChangeWord(word));
        }

        private static string ChangeWord(string s)
        {
            int upperCaseLetters = 0;
            int lowerCaseLetters = 0;

            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsUpper(s[i]))
                {
                    upperCaseLetters++;
                }
                else
                {
                    lowerCaseLetters++;
                }
            }

            if(upperCaseLetters > lowerCaseLetters)
            {
                return s.ToUpper();
            }
            else
            {
                return s.ToLower();
            }
        }
    }
}
