﻿using System;

namespace _01.Watermelon
{
    class Program
    {
        static void Main(string[] args)
        {
            int watermelonWeight = int.Parse(Console.ReadLine());

            if (watermelonWeight % 2 != 0 || watermelonWeight == 2)
            {
                Console.WriteLine("NO");
            }
            else
            {
                Console.WriteLine("YES");
            }
        }
    }
}
