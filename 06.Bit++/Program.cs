﻿using System;

namespace _06.Bit__
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int x = 0;

            for (int i = 0; i < n; i++)
            {
                string statement = Console.ReadLine();

                if (statement.Contains('-'))
                {
                    x--;
                }
                else
                {
                    x++;
                }
            }

            Console.WriteLine(x);
        }
    }
}
