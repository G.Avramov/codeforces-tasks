﻿using System;
using System.Linq;

namespace _12.BearAndBigBrother
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] limakAndBobWeight = Console.ReadLine()
                .Split(' ')
                .Select(int.Parse)
                .ToArray();

            int limakWeight = limakAndBobWeight[0];
            int bobWeight = limakAndBobWeight[1];

            int years = 0;

            while (true)
            {
                if(limakWeight > bobWeight)
                {
                    break;
                }

                limakWeight *= 3;
                bobWeight *= 2;
                years++;
            }

            Console.WriteLine(years);
        }
    }
}
