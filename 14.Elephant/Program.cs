﻿using System;

namespace _14.Elephant
{
    class Program
    {
        static void Main(string[] args)
        {
            int coordinate = int.Parse(Console.ReadLine());           

            Console.WriteLine(CalculateOptimalMoves(coordinate));            
        }

        private static int CalculateOptimalMoves(int coordinate)
        {
            int moves = 0;

            while (true)
            {
                if (coordinate == 0)
                {
                    break;
                }

                if (coordinate - 5 >= 0)
                {
                    moves++;
                    coordinate -= 5;
                }
                else if (coordinate - 4 >= 0)
                {
                    moves++;
                    coordinate -= 4;
                }
                else if (coordinate - 3 >= 0)
                {
                    moves++;
                    coordinate -= 3;
                }
                else if (coordinate - 2 >= 0)
                {
                    moves++;
                    coordinate -= 2;
                }
                else if (coordinate - 1 >= 0)
                {
                    moves++;
                    coordinate -= 1;
                }
            }

            return moves;
        }
    }
}
