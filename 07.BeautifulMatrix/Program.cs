﻿using System;
using System.Linq;

namespace _07.BeautifulMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] matrix = new int[5, 5];

            for (int i = 0; i < 5; i++)
            {
                var input = Console.ReadLine()
                    .Split(' ')
                    .Select(int.Parse)
                    .ToArray();

                for (int j = 0; j < 5; j++)
                {
                    matrix[i, j] = input[j];
                }
            }

            Console.WriteLine(CalculateMinumunMoves(matrix));
        }
        private static int FindRow(int[,] matrix)
        {
            int row = 0;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if(matrix[i,j] == 1)
                    {
                        row = i;
                        break;
                    }
                }
            }

            return row;
        }

        private static int FindCol(int[,] matrix)
        {
            int col = 0;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == 1)
                    {
                        col = j;
                        break;
                    }
                }
            }

            return col;
        }

        private static int CalculateMinumunMoves(int[,] matrix)
        {
            int moves = 0;

            int row = FindRow(matrix);
            int col = FindCol(matrix);

            if(row != 2)
            {
                if(row < 2)
                {
                    for (int i = row; i < 2; i++)
                    {
                        moves++;
                    }
                }
                else
                {
                    for (int i = row; i > 2; i--)
                    {
                        moves++;
                    }
                }
            }

            if (col != 2)
            {
                if (col < 2)
                {
                    for (int i = col; i < 2; i++)
                    {
                        moves++;
                    }
                }
                else
                {
                    for (int i = col; i > 2; i--)
                    {
                        moves++;
                    }
                }
            }

            return moves;
        }
    }
}
