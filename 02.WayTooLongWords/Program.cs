﻿using System;

namespace _02.WayTooLongWords
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            string[] words = new string[n];

            for (int i = 0; i < n; i++)
            {
                words[i] = Console.ReadLine();
                if (words[i].Length > 10)
                {
                    string temp = "";
                    temp += words[i][0] + (words[i].Length - 2).ToString() + words[i][words[i].Length - 1];
                    words[i] = temp;
                }
            }

            Console.WriteLine(string.Join('\n', words));
        }
    }
}
