﻿using System;
using System.Linq;

namespace _04.NextRound
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

            int participants = arr[0];
            int kFinisherPlace = arr[1];

            int[] scores = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();

            int nexRoundParticipants = 0;
            int scoreToCheck = scores[kFinisherPlace - 1];


            for (int i = 0; i < scores.Length; i++)
            {
                if (scores[i] >= scoreToCheck && scores[i] > 0)
                {
                    nexRoundParticipants++;
                }
            }

            Console.WriteLine(nexRoundParticipants);
        }
    }
}
