﻿using System;
using System.Linq;

namespace _16.WrongSubtraction
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input = Console.ReadLine()
                .Split(' ')
                .Select(int.Parse)
                .ToArray();

            Console.WriteLine(SubtractAsTania(input));
        }

        private static int SubtractAsTania(int[] input)
        {
            int number = input[0];
            int numOfSubtractions = input[1];

            while (numOfSubtractions > 0)
            {
                if(number % 10 == 0)
                {
                    number = number / 10;
                }
                else
                {
                    number--;
                }

                numOfSubtractions--;
            }

            return number;
        }
    }
}
