﻿using System;
using System.Linq;

namespace _13.SoldierAndBananas
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input = Console.ReadLine()
                .Split(' ')
                .Select(int.Parse)
                .ToArray();

            Console.WriteLine(MoneyToBorrow(input));
        }

        private static int MoneyToBorrow(int[] input)
        {
            int initialCost = input[0];
            int money = input[1];
            int bananas = input[2];

            int totalCost = 0;

            for (int i = 1; i <= bananas; i++)
            {
                totalCost += initialCost * i;
            }



            if(money >= totalCost)
            {
                return 0;
            }
            else
            {
                return totalCost - money;
            }
        }
    }
}
